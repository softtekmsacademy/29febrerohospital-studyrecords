CREATE TABLE `imaging_results` (
  `id_imaging_results` INT NOT NULL AUTO_INCREMENT,
  `interpretation` VARCHAR(1000) NULL DEFAULT NULL,
  PRIMARY KEY (`id_imaging_results`))
ENGINE = InnoDB;



CREATE TABLE `laboratory_results` (
  `id_laboratory_results` INT NOT NULL,
  `interpretation` VARCHAR(1000) NULL DEFAULT NULL,
  `sample` VARCHAR(30) NULL DEFAULT NULL,
  `sample_quantity` VARCHAR(30) NULL DEFAULT NULL,
  `rising_variables` VARCHAR(255) NULL DEFAULT NULL,
  `decline_variables` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id_laboratory_results`))
ENGINE = InnoDB;



CREATE TABLE `study_records` (
  `id_study_records` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NULL DEFAULT NULL,
  `accident` VARCHAR(1000) NULL DEFAULT NULL,
  `incident` VARCHAR(1000) NULL DEFAULT NULL,
  `clinical_problem` VARCHAR(1000) NULL DEFAULT NULL,
  `id_imaging_results` INT NULL,
  `id_laboratory_results` INT NULL,
  `nss` VARCHAR(10) NOT NULL,
  `id_medical_studies` INT NOT NULL,
  `id_employees` INT NOT NULL,
  PRIMARY KEY (`id_study_records`),
  CONSTRAINT `fk_StudyRecords_ImagingResults1`
    FOREIGN KEY (`id_imaging_results`)
    REFERENCES `imaging_results` (`id_imaging_results`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_StudyRecords_LaboratoryResults1`
    FOREIGN KEY (`id_laboratory_results`)
    REFERENCES `laboratory_results` (`id_laboratory_results`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



CREATE TABLE `images` (
  `id_images` INT NOT NULL AUTO_INCREMENT,
  `image_data` LONGBLOB NULL DEFAULT NULL,
  `id_imaging_results` INT NOT NULL,
  `image_name` CHAR(50) NULL DEFAULT NULL,
  `image_size` CHAR(50) NULL DEFAULT NULL,
  `image_type` CHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id_images`),
  CONSTRAINT `fk_Images_ImagingResults1`
    FOREIGN KEY (`id_imaging_results`)
    REFERENCES `imaging_results` (`id_imaging_results`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;ical_studies` INT NOT NULL,
  `id_employees` INT NOT NULL,
  PRIMARY KEY (`id_study_records`),
  INDEX `fk_StudyRecords_ImagingResults1_idx` (`id_imaging_results` ASC) VISIBLE,
  INDEX `fk_StudyRecords_LaboratoryResults1_idx` (`id_laboratory_results` ASC) VISIBLE,
  CONSTRAINT `fk_StudyRecords_ImagingResults1`
    FOREIGN KEY (`id_imaging_results`)
    REFERENCES `StudyRecords`.`imaging_results` (`id_imaging_results`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_StudyRecords_LaboratoryResults1`
    FOREIGN KEY (`id_laboratory_results`)
    REFERENCES `StudyRecords`.`laboratory_results` (`id_laboratory_results`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StudyRecords`.`images`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `StudyRecords`.`images` (
  `id_images` INT NOT NULL AUTO_INCREMENT,
  `image_data` LONGBLOB NULL DEFAULT NULL,
  `id_imaging_results` INT NOT NULL,
  `image_name` CHAR(50) NULL DEFAULT NULL,
  `image_size` CHAR(50) NULL DEFAULT NULL,
  `image_type` CHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id_images`),
  INDEX `fk_Images_ImagingResults1_idx` (`id_imaging_results` ASC) VISIBLE,
  CONSTRAINT `fk_Images_ImagingResults1`
    FOREIGN KEY (`id_imaging_results`)
    REFERENCES `StudyRecords`.`imaging_results` (`id_imaging_results`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
