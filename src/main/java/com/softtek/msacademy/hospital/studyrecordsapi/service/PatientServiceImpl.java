package com.softtek.msacademy.hospital.studyrecordsapi.service;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.hospital.studyrecordsapi.controller.StudyRecordsException;
import com.softtek.msacademy.hospital.studyrecordsapi.model.entity.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;


import java.util.List;
@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty( name = "coreSize", value = "20" ),
                @HystrixProperty( name = "maxQueueSize", value = "10") },
        commandProperties = {
                @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "30"),
                @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.timeInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.numBuckets", value = "10") } )
public class PatientServiceImpl implements PatientService {
    @Autowired
    private EurekaClient discoveryClient;
    private final RestTemplate call = new RestTemplate();

    public boolean patientExist(int id){
        String urlHostName="";
        List<Application> applications = discoveryClient.getApplications().getRegisteredApplications();

        for (Application application : applications) {
            List<InstanceInfo> applicationsInstances = application.getInstances();
            for (InstanceInfo applicationsInstance : applicationsInstances) {
                String name = applicationsInstance.getAppName();
                String url = applicationsInstance.getHomePageUrl();
                if(name.equals("PATIENTS-API") && url.contains("softtekmsacademypatients")){
                    urlHostName = applicationsInstance.getHostName();
                }
            }
        }
        String urlPatient="https://"+urlHostName+"/administration/patient/"+id;
        if(urlHostName==""){
            throw new StudyRecordsException("Patient service is not available");
        }
        Patient response = call.getForObject(urlPatient, Patient.class);
        if(response != null) {
            return true;
        }
        return false;
    }
}
