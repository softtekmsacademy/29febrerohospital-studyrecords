package com.softtek.msacademy.hospital.studyrecordsapi.model.entity;



import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.util.*;

public class Patient implements Serializable{
    //Empty Constructor
    public Patient(){}

    //Main fields of table
    private Long nss;
    private String first_name;
    private String last_name;
    private String second_last_name;
    private Date birth_date;
    private String gender;


    private Integer health_record;

    public Long getNss() {
        return nss;
    }

    //GET & SET
    public void setNss(Long nss) {
        this.nss = nss;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getSecond_last_name() {
        return second_last_name;
    }

    public void setSecond_last_name(String second_last_name) {
        this.second_last_name = second_last_name;
    }

    public Date getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(Date birth_date) {
        this.birth_date = birth_date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getHealth_record() {
        return health_record;
    }

    public void setHealth_record(Integer health_record) {
        this.health_record = health_record;
    }

  /*  @Override
    public String toString() {
        return "Patient{" +
                "nss=" + nss +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", second_last_name='" + second_last_name + '\'' +
                ", birth_date=" + birth_date +
                ", gender='" + gender + '\'' +
                ", fromFiscalData=" + fromFiscalData +
                ", bloodGroup=" + bloodGroup +
                ", birth_place=" + state +
                ", nationality=" + nationality +
                ", religion=" + religion +
                ", civilStatus=" + civilStatus +
                ", health_record=" + health_record +
                '}';
    }*/
}
