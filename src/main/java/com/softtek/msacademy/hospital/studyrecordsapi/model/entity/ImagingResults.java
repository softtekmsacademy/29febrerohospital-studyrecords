package com.softtek.msacademy.hospital.studyrecordsapi.model.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "imaging_results")
public class ImagingResults extends Result {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_imaging_results")
    private int id;
    @OneToMany(mappedBy = "imagingResults")
    private Set<Image> images;

    public ImagingResults() {

    }

    public ImagingResults(String interpretation) {
        super(interpretation);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }
}