package com.softtek.msacademy.hospital.studyrecordsapi.controller;

import com.softtek.msacademy.hospital.studyrecordsapi.model.entity.StudyRecords;

public class StudyRecordsException extends RuntimeException{
    public StudyRecordsException(String exception){
        super(exception);
    }
}
