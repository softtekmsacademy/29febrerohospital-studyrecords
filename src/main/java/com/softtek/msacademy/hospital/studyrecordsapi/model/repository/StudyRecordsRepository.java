package com.softtek.msacademy.hospital.studyrecordsapi.model.repository;

import com.softtek.msacademy.hospital.studyrecordsapi.model.entity.StudyRecords;

import java.util.List;

public interface StudyRecordsRepository extends org.springframework.data.jpa.repository.JpaRepository<StudyRecords, Long>  {
    public List<StudyRecords> findByNss(String patientId);
    public StudyRecords findById(int Id);
    public List<StudyRecords> findAll();

}
