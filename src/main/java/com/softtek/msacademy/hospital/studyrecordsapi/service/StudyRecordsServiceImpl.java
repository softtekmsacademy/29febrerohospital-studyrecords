package com.softtek.msacademy.hospital.studyrecordsapi.service;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.hospital.studyrecordsapi.controller.StudyRecordsException;
import com.softtek.msacademy.hospital.studyrecordsapi.model.entity.StudyRecords;
import com.softtek.msacademy.hospital.studyrecordsapi.model.repository.StudyRecordsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty(name = "coreSize", value = "10"),
                @HystrixProperty(name = "maxQueueSize", value = "-1")
        },
        commandProperties = {
                @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "20"),
                @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
                @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "10000"),
                @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "10")
        }
)
public class StudyRecordsServiceImpl implements StudyRecordsService{
    @Autowired
    StudyRecordsRepository studyRecordsRepository;

    @HystrixCommand(threadPoolKey = "addThreadPool")
    @Override
    public StudyRecords addStudyRecord(StudyRecords studyRecords){
        return studyRecordsRepository.save(studyRecords);
    }
    @HystrixCommand(threadPoolKey = "getThreadPool")
    @Override
    public StudyRecords getStudyRecordbyId(int id){
        Optional<StudyRecords> studyRecordOb = Optional.ofNullable(this.studyRecordsRepository.findById(id));
        if(!studyRecordOb.isPresent())
            throw new StudyRecordsException("A record with that ID does not exist");
        return studyRecordOb.get();
    }
    public List<StudyRecords> getStudyRecordbyNss(String nss){
        Optional<List<StudyRecords>> studyRecordOb = Optional.ofNullable(this.studyRecordsRepository.findByNss(nss));
        if(!studyRecordOb.isPresent())
            throw new StudyRecordsException("A record of that patient does not exist");
        return studyRecordOb.get();
    }
    public List<StudyRecords> getAllStudyRecords(){
        Optional<List<StudyRecords>> studyRecordOb = Optional.ofNullable(this.studyRecordsRepository.findAll());
        if(!studyRecordOb.isPresent())
            throw new StudyRecordsException("A record with that ID does not exist");
        return studyRecordOb.get();
    }
}
