package com.softtek.msacademy.hospital.studyrecordsapi.model.entity;

import javax.persistence.*;

@Entity
@Table(name="laboratory_results")
public class LaboratoryResults extends Result{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_laboratory_results")
    protected int id;
    @Column(name = "sample")
    private String sample;
    @Column(name = "sample_quantity")
    private String sampleQuantity;
    @Column(name = "rising_variables")
    private String risingVariables;
    @Column(name = "decline_variables")
    private String declineVariables;

    public LaboratoryResults() {

    }

    public LaboratoryResults(String sample, String sampleQuantity, String risingVariables, String declineVariables) {
        this.sample = sample;
        this.sampleQuantity = sampleQuantity;
        this.risingVariables = risingVariables;
        this.declineVariables = declineVariables;
    }

    public LaboratoryResults(String interpretation, String sample, String sampleQuantity, String risingVariables, String declineVariables) {
        super(interpretation);
        this.sample = sample;
        this.sampleQuantity = sampleQuantity;
        this.risingVariables = risingVariables;
        this.declineVariables = declineVariables;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    public String getSampleQuantity() {
        return sampleQuantity;
    }

    public void setSampleQuantity(String sampleQuantity) {
        this.sampleQuantity = sampleQuantity;
    }

    public String getRisingVariables() {
        return risingVariables;
    }

    public void setRisingVariables(String risingVariables) {
        this.risingVariables = risingVariables;
    }

    public String getDeclineVariables() {
        return declineVariables;
    }

    public void setDeclineVariables(String declineVariables) {
        this.declineVariables = declineVariables;
    }

}
