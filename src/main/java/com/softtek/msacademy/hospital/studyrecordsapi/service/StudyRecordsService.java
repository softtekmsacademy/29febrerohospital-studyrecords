package com.softtek.msacademy.hospital.studyrecordsapi.service;

import com.softtek.msacademy.hospital.studyrecordsapi.model.entity.StudyRecords;

import java.util.List;

public interface StudyRecordsService {
    StudyRecords addStudyRecord(StudyRecords studyRecords);
    StudyRecords getStudyRecordbyId(int id);
    List<StudyRecords> getStudyRecordbyNss(String nss);
    List<StudyRecords> getAllStudyRecords();
}
