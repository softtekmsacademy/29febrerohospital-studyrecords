package com.softtek.msacademy.hospital.studyrecordsapi.model.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Result {

    @Column(name = "interpretation")
    protected String interpretation;

    public Result() {
    }

    public Result( String interpretation) {
        this.interpretation = interpretation;
    }
    public String getInterpretation() {
        return interpretation;
    }

    public void setInterpretation(String interpretation) {
        this.interpretation = interpretation;
    }
}
