package com.softtek.msacademy.hospital.studyrecordsapi.service;

public interface PatientService {
    boolean patientExist(int id);
}
