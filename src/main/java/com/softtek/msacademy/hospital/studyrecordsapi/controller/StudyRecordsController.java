package com.softtek.msacademy.hospital.studyrecordsapi.controller;

import com.netflix.discovery.EurekaClient;
import com.softtek.msacademy.hospital.studyrecordsapi.model.entity.StudyRecords;
import com.softtek.msacademy.hospital.studyrecordsapi.model.repository.StudyRecordsRepository;
import com.softtek.msacademy.hospital.studyrecordsapi.service.EmployeeService;
import com.softtek.msacademy.hospital.studyrecordsapi.service.MedicalStudyService;
import com.softtek.msacademy.hospital.studyrecordsapi.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class StudyRecordsController {
    private final ObjectMapper objectMapper;
    @Autowired
    private StudyRecordsRepository studyRecordsRepository;
    public StudyRecordsController(ObjectMapper objectMapper){
        this.objectMapper=objectMapper;
    }
    private final RestTemplate call = new RestTemplate();
    @Autowired
    private EurekaClient discoveryClient;
    @Autowired
    private MedicalStudyService medicalStudyService;
    @Autowired
    private PatientService patientService;
    @Autowired
    private EmployeeService employeeService;

    @GetMapping(value="/studyRecords/{nss}", produces = "application/json")
    public ResponseEntity<String> getStudyRecord(@PathVariable String nss){
            if(studyRecordsRepository.findByNss(nss).isEmpty()){
                throw new StudyRecordsException("No study records found for that patient");
            }
            else {
                return new ResponseEntity(studyRecordsRepository.findByNss(nss), HttpStatus.OK);
            }
    }

    @GetMapping(value="/studyRecord/{id}", produces = "application/json")
    public ResponseEntity getStudyRecordbyId(@PathVariable int id) {
            if(studyRecordsRepository.findById(id)==null){
                throw new StudyRecordsException("Study record with that id does not exist");
            }
            else {
                return new ResponseEntity(studyRecordsRepository.findById(id), HttpStatus.OK);
            }
    }

    @GetMapping(value="/studyRecords", produces = "application/json")
    public ResponseEntity getAllRecords() {
            if(studyRecordsRepository.findAll().isEmpty()){
                throw new StudyRecordsException("No study records found");
            }
            else {
                return new ResponseEntity(studyRecordsRepository.findAll(), HttpStatus.OK);
            }
    }




    @PostMapping(value="/studyRecords", consumes = "application/json")
     public ResponseEntity<StudyRecords> addStudyRecord(@Valid @RequestBody StudyRecords studyRecords){
        Optional<StudyRecords> studyRecords1=Optional.ofNullable(studyRecordsRepository.findById(studyRecords.getId()));
        if(studyRecords1.isPresent()) {
            throw new StudyRecordsException("A study record with that id already exists");
        }
        if(!medicalStudyService.medicalStudyExist(studyRecords.getMedicalStudy())) {
            throw new StudyRecordsException("Connection with medical studies refused");
        }
        if(!employeeService.employeeExist(studyRecords.getResponsible())){
            throw new StudyRecordsException("Connection with employees refused");
        }
        if(!patientService.patientExist(Integer.valueOf(studyRecords.getPatient()))){
            throw new StudyRecordsException("Connection with patient refused");
        }
        else{
            studyRecordsRepository.save(studyRecords);
            return ResponseEntity.ok().body(studyRecords);
        }

    }

}
