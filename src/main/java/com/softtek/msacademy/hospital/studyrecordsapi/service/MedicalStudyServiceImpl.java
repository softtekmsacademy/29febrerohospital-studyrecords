package com.softtek.msacademy.hospital.studyrecordsapi.service;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.hospital.studyrecordsapi.controller.StudyRecordsException;
import com.softtek.msacademy.hospital.studyrecordsapi.model.entity.MedicalStudy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;


import java.util.List;
@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty( name = "coreSize", value = "20" ),
                @HystrixProperty( name = "maxQueueSize", value = "10") },
        commandProperties = {
                @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "30"),
                @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.timeInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.numBuckets", value = "10") } )
public class MedicalStudyServiceImpl implements MedicalStudyService {
    @Autowired
    private EurekaClient discoveryClient;
    private final RestTemplate call = new RestTemplate();

   public boolean medicalStudyExist(int id){
       String urlHostName="";
       List<Application> applications = discoveryClient.getApplications().getRegisteredApplications();

       for (Application application : applications) {
           List<InstanceInfo> applicationsInstances = application.getInstances();
           for (InstanceInfo applicationsInstance : applicationsInstances) {
               String name = applicationsInstance.getAppName();
               String url = applicationsInstance.getHomePageUrl();
               if(name.equals("MEDICALSTUDY-API") && url.contains("medicalstudies")){
                   urlHostName = applicationsInstance.getHostName();
               }
           }
       }
       String urlMedicalStudy="https://"+urlHostName+"/medicalStudies/"+id;
       if(urlHostName==""){
           throw new StudyRecordsException("Medical study service is not available");
       }
       MedicalStudy response = call.getForObject(urlMedicalStudy, MedicalStudy.class);
       if(response != null) {
           return true;
       }
       return false;
   }
}
