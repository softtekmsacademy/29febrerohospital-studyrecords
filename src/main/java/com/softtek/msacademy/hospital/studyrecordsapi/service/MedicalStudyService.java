package com.softtek.msacademy.hospital.studyrecordsapi.service;

public interface MedicalStudyService {
    boolean medicalStudyExist(int id);
}
