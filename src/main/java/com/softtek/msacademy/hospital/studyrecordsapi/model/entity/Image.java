package com.softtek.msacademy.hospital.studyrecordsapi.model.entity;

import javax.persistence.*;
import java.awt.image.BufferedImage;

@Entity
@Table(name="images")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_images")
    private int id;
    @ManyToOne
    @JoinColumn(name="id_imaging_results")
    private ImagingResults imagingResults;
    /*@Column(name="image_data")
    private BufferedImage image;*/
    @Column(name="image_name")
    private String name;
    @Column(name="image_size")
    private String size;
    @Column(name="image_type")
    private String type;

    public Image() {
    }

    public Image(BufferedImage image, String name, String size, String type) {
        //this.image = image;
        this.name = name;
        this.size = size;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /*public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
