package com.softtek.msacademy.hospital.studyrecordsapi.service;

public interface EmployeeService {
    boolean employeeExist(int id);
}
