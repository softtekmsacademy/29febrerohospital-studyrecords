package com.softtek.msacademy.hospital.studyrecordsapi.model.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "study_records")
public class StudyRecords {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_study_records")
    private int id;
    @Column(name = "date")
    private Date date;
    @Column(name = "incident")
    private String indicents;
    @Column(name = "accident")
    private String accidents;
    @Column(name = "clinical_problem")
    private String clinical_problem;
    @ManyToOne
    @JoinColumn(name = "id_laboratory_results")
    private LaboratoryResults laboratoryResults;
    @ManyToOne
    @JoinColumn(name = "id_imaging_results")
    private ImagingResults imagingResults;
    @JoinColumn(name = "id_employees")
    private int id_employees;
    @JoinColumn(name = "id_medical_studies")
    private int id_medical_studies;
    @JoinColumn(name = "nss")
    private String nss;

    public StudyRecords() {
    }

    public StudyRecords(Date date, String indicents, String accidents, String clinical_problem, LaboratoryResults laboratoryResults,  int id_employees, int id_medical_studies, String nss) {
        this.date = date;
        this.indicents = indicents;
        this.accidents = accidents;
        this.clinical_problem = clinical_problem;
        this.laboratoryResults = laboratoryResults;
        this.imagingResults = imagingResults;
        this.id_employees = id_employees;
        this.id_medical_studies = id_medical_studies;
        this.nss = nss;
    }
    public StudyRecords(Date date, String indicents, String accidents, String clinical_problem, ImagingResults imagingResults, int id_employees, int id_medical_studies, String nss) {
        this.date = date;
        this.indicents = indicents;
        this.accidents = accidents;
        this.clinical_problem = clinical_problem;
        this.laboratoryResults = laboratoryResults;
        this.imagingResults = imagingResults;
        this.id_employees = id_employees;
        this.id_medical_studies = id_medical_studies;
        this.nss = nss;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getIndicents() {
        return indicents;
    }

    public void setIndicents(String indicents) {
        this.indicents = indicents;
    }

    public String getAccidents() {
        return accidents;
    }

    public void setAccidents(String accidents) {
        this.accidents = accidents;
    }

    public String getClinical_problem() {
        return clinical_problem;
    }

    public void setClinical_problem(String clinical_problem) {
        this.clinical_problem = clinical_problem;
    }

    public LaboratoryResults getLaboratoryResults() {
        return laboratoryResults;
    }

    public void setLaboratoryResults(LaboratoryResults laboratoryResults) {
        this.laboratoryResults = laboratoryResults;
    }

    public ImagingResults getImagingResults() {
        return imagingResults;
    }

    public void setImagingResults(ImagingResults imagingResults) {
        this.imagingResults = imagingResults;
    }

    public int getResponsible() {
        return id_employees;
    }

    public void setResponsible(int id_employees) {
        this.id_employees = id_employees;
    }

    public int getMedicalStudy() {
        return id_medical_studies;
    }

    public void setMedicalStudy(int id_medical_studies) {
        this.id_medical_studies = id_medical_studies;
    }

    public String getPatient() {
        return nss;
    }

    public void setPatient(String nss) {
        this.nss = nss;
    }
}

