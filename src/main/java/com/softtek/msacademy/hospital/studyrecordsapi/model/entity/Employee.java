package com.softtek.msacademy.hospital.studyrecordsapi.model.entity;

import java.util.Date;

public class Employee {
    private Integer id_employee;
    private String first_name;
    private String last_name;
    private String second_last_name;
   /* private Role role;
    private EmployeeType type;
    private FiscalData fiscal_data;*/
    private Date birth_date;
    //private Speciality speciality;

    public Employee(int id_employee, String first_name,String last_name, String second_last_name,
                     Date birth_date) {
        this.id_employee = id_employee;
        this.first_name = first_name;
        this.last_name=last_name;
        this.second_last_name = second_last_name;
        this.birth_date = birth_date;
    }


    public Employee() {
    }


    public int getId_employee() {
        return id_employee;
    }

    public void setId_employee(int id_employee) {
        this.id_employee = id_employee;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }


    public String getLast_name() {
        return last_name;
    }


    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }


    public String getSecond_last_name() {
        return second_last_name;
    }

    public void setSecond_last_name(String second_last_name) {
        this.second_last_name = second_last_name;
    }

    public Date getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(Date birth_date) {
        this.birth_date = birth_date;
    }


}
